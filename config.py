FZF = "fzf --reverse < FILE"
BROWSER = "palemoon"
EDITOR = "emacs"
PLAYER = "mpv"
IMAGE = "mpv"# You can change this to a normal image viewer
TORRENT = "aria2c"
